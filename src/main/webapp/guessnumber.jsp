<%@ page import="java.util.List" %>
<%@ page import="com.hashted.examples.guessthenumber.entity.guessnumber.Attempt" %>
<%@ page import="com.hashted.examples.guessthenumber.entity.guessnumber.AttemptHTMLMapping" %>
<%--
  Created by IntelliJ IDEA.
  User: hashted
  Date: 18.09.2018
  Time: 14:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
  <head>
      <title>Game - Guess the Number!</title>
      <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="css/main.css">
      <link rel="stylesheet" type="text/css" href="css/callout.css">
      <link rel="stylesheet" type="text/css" href="css/numpad.css">
  </head>
  <body>

    <nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
      <div class="container">
          <div class="navbar-brand">Guess the Number</div>
          <div class="collapse navbar-collapse">
              <ul class="navbar-nav">
                  <li class="nav-item">
                      <%
                          if (request.getAttribute("webRoot") != null) {
                              out.println("<a class=\"nav-link\" href=\"" +
                                      request.getAttribute("webRoot") + "/guessnumber\">Home</a>");
                          }
                      %>
                  </li>
                  <li class="nav-item">
                      <%
                          if (request.getAttribute("webRoot") != null)
                              out.println("<a class=\"nav-link\" href=\"" +
                                      request.getAttribute("webRoot") + "/rating\">Rating</a>");
                      %>
                  </li>
              </ul>
          </div>
          <form class="form-inline">
              <%
                  if (request.getAttribute("userName") != null)
                      out.println("<span style=\"margin: 0 15px;\">" + request.getAttribute("userName") + "</span>");

                  if (request.getAttribute("webRoot") != null) {
                      out.println("<a class=\"text-primary\" href=\"" +
                              request.getAttribute("webRoot") + "/logout\" style=\"text-decoration: none;\">Sign Out</a>");
                  }
              %>
          </form>
      </div>
    </nav>

    <div class="inner">
        <h1>Guess the Number!</h1>
        <form action="generate" class="form-group" method="post">
            <button type="submit" class="btn btn-success">Generate</button>
        </form>
        <form action="check" class="form-group" method="post">
            <%
                if (request.getAttribute("inputEnabled") != null)
                    out.println("<fieldset>");
                else
                    out.println("<fieldset disabled=\"disabled\">");
            %>
                <input class="form-control check-form__input"
                       name="number" type="number" min="1023" max="9876"  required="required"
                       rel="txtTooltip" data-toggle="tooltip" data-placement="top" title="Enter your number">
                <button type="submit" class="btn btn-primary check-form__button">Check</button>
            </fieldset>
        </form>

        <div class="numpad">
            <div class="numpad__container">
                <div>
                    <button type="button" class="btn btn-light numpad__btn numpad__btn_number">7</button>
                    <button type="button" class="btn  btn-light numpad__btn numpad__btn_number">8</button>
                    <button type="button" class="btn  btn-light numpad__btn numpad__btn_number">9</button>
                </div>
                <div>
                    <button type="button" class="btn btn-light numpad__btn numpad__btn_number">4</button>
                    <button type="button" class="btn btn-light numpad__btn numpad__btn_number">5</button>
                    <button type="button" class="btn btn-light numpad__btn numpad__btn_number">6</button>
                </div>
                <div>
                    <button type="button" class="btn btn-light numpad__btn numpad__btn_number">1</button>
                    <button type="button" class="btn btn-light numpad__btn numpad__btn_number">2</button>
                    <button type="button" class="btn btn-light numpad__btn numpad__btn_number">3</button>
                </div>
                <div>
                    <button type="button" class="btn btn-success numpad__btn numpad__btn_delete">del</button>
                    <button type="button" class="btn btn-light numpad__btn numpad__btn_number">0</button>
                    <button type="button" class="btn btn-success numpad__btn numpad__btn_clear">c</button>
                </div>
            </div>
        </div>

        <%
            if (request.getAttribute("numberIsGenerated") != null) {
                out.println(
                    "<div class=\"bs-callout bs-callout-info\"><div>" +
                        "<h4>The new digit number was generated.</h4>" +
                        "<p>Try to guess it! You have an infinity number of attempts. Notice that the number doesn't have repeated digits.</p>" +
                    "</div></div>"
                );
            }
            if (request.getAttribute("invalidNumber") != null) {
                out.println(
                        "<div class=\"bs-callout bs-callout-warning\"><div>" +
                                "<h4>Invalid number.</h4>" +
                                "<p>It seems you entered an incorrect value.</p>" +
                                "</div></div>"
                );
            }
            if (request.getAttribute("currentStatus") != null) {
                out.println(AttemptHTMLMapping.statusMessage.get(request.getAttribute("currentStatus")));
            }
        %>
        <div class="table__outer">
            <table class="table table-condensed table__attempts">
            <%
                List<Attempt> attempts = (List<Attempt>) request.getAttribute("attemptList");

                if (attempts != null && !attempts.isEmpty()) {
                    out.println("<thead><tr><td>Value</td>" +
                            "<td data-toggle=\"tooltip\"" +
                            "title=\"B (bulls) - the number of digits in the guess that are in the correct position. " +
                            "С (cows) - the number of digits that are guessed correctly but are not in the correct position.\">" +
                            "Result</td><td>Status</td></tr></thead>");
                    out.println("<tbody>");
                    for (int i = attempts.size() - 1; i >= 0; i--) {
                        Attempt a = attempts.get(i);
                        StringBuilder attemptTable = new StringBuilder();
                        attemptTable
                                .append("<tr class=\"")
                                .append(AttemptHTMLMapping.classAttrString.get(a.getStatus())).append("\">")
                                .append("<td>").append(a.getValue()).append("</td>")
                                .append("<td>").append(a.getCheckResult()).append("</td>")
                                .append("<td style=\"color:").append(AttemptHTMLMapping.attemptColorCode.get(a.getStatus()))
                                .append("\">").append(AttemptHTMLMapping.textStatus.get(a.getStatus())).append("</td>")
                                .append("</tr>");
                        out.println(attemptTable);
                    }
                    out.println("</tbody>");
                }
            %>
            </table>
        </div>
    </div>
    <script src="bootstrap/js/jquery-3.3.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $('input[rel="txtTooltip"]').tooltip();

            $(".numpad").hide();

            $('.check-form__input').click(function(){
                $('.numpad').toggle();
            });

            $(document).click(function(){
                if (!$(event.target).hasClass('check-form__input') &&
                    !$(event.target).is('.numpad *') ) {
                    $(".numpad").hide();
                }
            });

            $('.numpad__btn_number').click(function(){
                $('.check-form__input').val($('.check-form__input').val() + $(this).text());
            });
            $('.numpad__btn_delete').click(function(){
                $('.check-form__input').val($('.check-form__input').val().substring(0,$('.check-form__input').val().length - 1));
            });
            $('.numpad__btn_clear').click(function(){
                $('.check-form__input').val('');
            });
        });
    </script>
  </body>
</html>
