package com.hashted.examples.guessthenumber.model;


public final class Model {

    private Model() {
    }

    private static class SingletonHolder { // "Initialization on Demand Holder"
        private  final static Model instance = new Model();
    }

    public static Model getInstance() {
        return SingletonHolder.instance;
    }
}
