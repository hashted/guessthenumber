package com.hashted.examples.guessthenumber.model;

import javax.servlet.http.HttpSession;

public class SessionUtils {

    public <T> T getSessionValue(HttpSession httpSession, final String key) {
        try {
            T value = (T) httpSession.getAttribute(key);
            return value;
        } catch (ClassCastException e) {
            return null;
        }
    }

    public <T> void setSessionValue(HttpSession httpSession, final String key, final T value) {
        httpSession.setAttribute(key, value);
    }
}
