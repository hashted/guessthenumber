package com.hashted.examples.guessthenumber.service;

import com.hashted.examples.guessthenumber.entity.RatingInfo;
import com.hashted.examples.guessthenumber.entity.Stat;

import java.util.List;

public interface StatService {

    void addStat(Stat stat);
    List<RatingInfo> getRatingList();
    double getAvgNumOfAttempts(int user_id);
}
