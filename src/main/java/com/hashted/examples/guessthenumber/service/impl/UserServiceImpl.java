package com.hashted.examples.guessthenumber.service.impl;

import com.hashted.examples.guessthenumber.entity.Role;
import com.hashted.examples.guessthenumber.entity.User;
import com.hashted.examples.guessthenumber.repository.RoleRepository;
import com.hashted.examples.guessthenumber.repository.UserRepository;
import com.hashted.examples.guessthenumber.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;

@Service("userServiceImpl")
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(@Qualifier("userRepository") UserRepository userRepository,
                           @Qualifier("roleRepository") RoleRepository roleRepository,
                           BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public User findUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public void saveUser(User user) {
        user.setPass(bCryptPasswordEncoder.encode(user.getPass())); //see WebMvcConfig
        user.setActive(1);
        Role userRole = roleRepository.findByRole("USER");
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));

        userRepository.save(user);
    }
}
