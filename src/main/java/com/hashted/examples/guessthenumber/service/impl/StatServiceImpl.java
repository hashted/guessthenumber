package com.hashted.examples.guessthenumber.service.impl;

import com.hashted.examples.guessthenumber.entity.RatingInfo;
import com.hashted.examples.guessthenumber.entity.Stat;
import com.hashted.examples.guessthenumber.repository.StatRepository;
import com.hashted.examples.guessthenumber.service.StatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StatServiceImpl implements StatService {

    StatRepository statRepository;

    @Autowired
    public StatServiceImpl(StatRepository statRepository) {
        this.statRepository = statRepository;
    }

    @Override
    public void addStat(Stat stat) {
        statRepository.saveAndFlush(stat);
    }

    @Override
    public List<RatingInfo> getRatingList(){
        List<Object[]> tuples = statRepository.getTopByAvg(10);
        List<RatingInfo> ratinglist = new ArrayList<>();

        for (Object[] tuple: tuples) {
            String name = (String) tuple[0];
            double avgNumOfAttempts = (double) tuple[1];
            int place = (int) tuple[2];
            ratinglist.add(new RatingInfo(name, avgNumOfAttempts, place));
        }
        return ratinglist;
    }

    @Override
    public double getAvgNumOfAttempts(int user_id) {
        Object obj = statRepository.getAvgByUserId(user_id);
        return obj == null ? 0 : (double) obj;
    }
}
