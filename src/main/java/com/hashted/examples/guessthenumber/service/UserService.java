package com.hashted.examples.guessthenumber.service;

import com.hashted.examples.guessthenumber.entity.User;

public interface UserService {

    User findUserByLogin(String login);
    void saveUser(User user);
}
