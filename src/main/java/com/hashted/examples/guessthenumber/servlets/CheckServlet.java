package com.hashted.examples.guessthenumber.servlets;

import com.hashted.examples.guessthenumber.entity.Stat;
import com.hashted.examples.guessthenumber.entity.User;
import com.hashted.examples.guessthenumber.entity.guessnumber.Attempt;
import com.hashted.examples.guessthenumber.entity.guessnumber.AttemptStatus;
import com.hashted.examples.guessthenumber.entity.guessnumber.Checker;
import com.hashted.examples.guessthenumber.service.StatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Component
public class CheckServlet extends ExtendedHttpServletWithAutowiredCapability {

    // The autowired annotation does not work out of the box in servlets.
    // See ExtendedHttpServletWithAutowiredCapability class with the overrided init() method within.
    @Autowired
    private StatService statService;

    @Autowired
    private HttpSession httpSession;

    @Value("${web.root}")
    private String webRoot;

//    Attempt a;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User user = (User) httpSession.getAttribute("user");
        List<Attempt> attemptList = (List<Attempt>) httpSession.getAttribute("attemptList");

        if (request.getParameter("status") != null &&
                request.getParameter("status").equals("success")) {
            Attempt a = (Attempt) httpSession.getAttribute("currentAttempt");

            if (a.getStatus().equals(AttemptStatus.CORRECT)) {
                Stat stat = new Stat();
                stat.setAttemptsQuantity(attemptList.size());
                stat.setUserId(user.getId());
                statService.addStat(stat);
            } else
                request.setAttribute("inputEnabled", true);

            request.setAttribute("currentStatus", a.getStatus());
        } else if (request.getParameter("status") != null &&
                request.getParameter("status").equals("invalid_number")) {
            request.setAttribute("inputEnabled", true);
            request.setAttribute("invalidNumber", true);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("error.html");
            rd.forward(request, response);
        }

        request.setAttribute("webRoot", webRoot);
        request.setAttribute("userName", user.getLogin());
        request.setAttribute("attemptList", attemptList);

        RequestDispatcher rd = request.getRequestDispatcher("guessnumber.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Integer> generatedList = (List<Integer>) httpSession.getAttribute("generatedList");
        List<Attempt> attemptList = (List<Attempt>) httpSession.getAttribute("attemptList");

        try {
            int number = Integer.valueOf(request.getParameter("number"));
            Attempt a = Checker.checkUserAttempt(generatedList, number);
            attemptList.add(a);
            httpSession.setAttribute("currentAttempt", a);
            response.sendRedirect(webRoot + "/check?status=success");
        } catch (NumberFormatException e) {
            response.sendRedirect(webRoot + "/check?status=invalid_number");
        }
    }
}