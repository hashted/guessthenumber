package com.hashted.examples.guessthenumber.servlets;

import com.hashted.examples.guessthenumber.entity.User;
import com.hashted.examples.guessthenumber.entity.guessnumber.Attempt;
import com.hashted.examples.guessthenumber.entity.guessnumber.Generator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Component
@Slf4j
public class GenerateServlet extends ExtendedHttpServletWithAutowiredCapability {

    @Autowired
    private HttpSession httpSession;

    @Value("${web.root}")
    private String webRoot;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User user = (User) httpSession.getAttribute("user");

        request.setAttribute("webRoot", webRoot);

        if(request.getParameter("status") != null &&
                request.getParameter("status").equals("success")) {
            request.setAttribute("userName", user.getLogin());
            request.setAttribute("numberIsGenerated", true);
            request.setAttribute("inputEnabled", true);

            RequestDispatcher rd = request.getRequestDispatcher("guessnumber.jsp");
            rd.forward(request, response);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("error.html");
            rd.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Attempt> attemptList = (List<Attempt>) httpSession.getAttribute("attemptList");
        attemptList.clear();

        int numberLength= 4;
        Generator gen = new Generator(numberLength);
        httpSession.setAttribute("generatedList", gen.generate());

        List<Integer> generatedList = (List<Integer>) httpSession.getAttribute("generatedList");

        if (generatedList != null && generatedList.size() == numberLength) {
            log.info("HttpSession id " + httpSession.getId() + " : " +
                    "New number was generated '" + generatedList + "'");
            response.sendRedirect(webRoot + "/generate?status=success");
        }
        else {
            log.warn("Could not generate a new number");
            response.sendRedirect(webRoot + "/generate?status=error");
        }
    }
}
