package com.hashted.examples.guessthenumber.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "stat")
public class Stat {

    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "attempts_quantity", nullable = false)
    private int attemptsQuantity;

    @Column(name = "user_id", nullable = false)
    private int userId;
}
