package com.hashted.examples.guessthenumber.entity.guessnumber;

import java.util.*;

public class Generator {
    private int length;

    public Generator(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public List<Integer> generate() {
        Random rand = new Random();
        HashSet<Integer> set = new HashSet(length);
        while (set.size() < length) {
            set.add(rand.nextInt(10));
        }

        List<Integer> list = new ArrayList(set);
        do {
            Collections.shuffle(list);
        } while (list.get(0) == 0);

        return list;
    }
}
