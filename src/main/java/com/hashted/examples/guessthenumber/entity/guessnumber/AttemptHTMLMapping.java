package com.hashted.examples.guessthenumber.entity.guessnumber;

import java.util.HashMap;
import java.util.Map;

public class AttemptHTMLMapping {
    public static final Map<AttemptStatus, String> statusMessage = new HashMap<AttemptStatus, String>(){
        {
            put(AttemptStatus.CORRECT, "<div class=\"bs-callout bs-callout-limegreen\"><div><h4>Congratulations!</h4><p>This is the correct answer!</p></div></div>");
            put(AttemptStatus.WRONG, "<div class=\"bs-callout bs-callout-info\"><div><h4>Sorry, but this is the wrong answer!</h4><p>Don't give up. You have an infinity number of attempts.</p></div></div>");
            put(AttemptStatus.REPEATED_DIGITS, "<div class=\"bs-callout bs-callout-danger\"><div><h4>Repeated digits!</h4><p>Notice that the number mustn't have repeated digits.</p></div></div>");
            put(AttemptStatus.INVALID_LENGTH, "<div class=\"bs-callout bs-callout-danger\"><div><h4>Invalid length!</h4><p>The number must contain 4 digits.</p></div></div>");
            put(AttemptStatus.UNEXPECTED_RESULT, "<div class=\"bs-callout bs-callout-warning\"><div><h4>Unexpected result!</h4><p>No one knows what happened. Try again, maybe it will correct itself.</p></div></div>");
        }
    };

    public static final Map<AttemptStatus, String> classAttrString = new HashMap<AttemptStatus, String>(){
        {
            put(AttemptStatus.CORRECT, "bs-callout bs-callout-limegreen");
            put(AttemptStatus.WRONG, "bs-callout bs-callout-info");
            put(AttemptStatus.REPEATED_DIGITS, "bs-callout bs-callout-danger");
            put(AttemptStatus.INVALID_LENGTH, "bs-callout bs-callout-danger");
            put(AttemptStatus.UNEXPECTED_RESULT, "bs-callout bs-callout-warning");
        }
    };

    public static final Map<AttemptStatus, String> textStatus = new HashMap<AttemptStatus, String>(){
        {
            put(AttemptStatus.CORRECT, "Correct");
            put(AttemptStatus.WRONG, "Wrong");
            put(AttemptStatus.REPEATED_DIGITS, "Repeated digits");
            put(AttemptStatus.INVALID_LENGTH, "Invalid length");
            put(AttemptStatus.UNEXPECTED_RESULT, "Unexpected result");
        }
    };

    public static final Map<AttemptStatus, String> attemptColorCode = new HashMap<AttemptStatus, String>(){
        {
            put(AttemptStatus.CORRECT, "#32cd32");
            put(AttemptStatus.WRONG, "#5bc0de");
            put(AttemptStatus.REPEATED_DIGITS, "#d9534f");
            put(AttemptStatus.INVALID_LENGTH, "#d9534f");
            put(AttemptStatus.UNEXPECTED_RESULT, "#d9534f");
        }
    };
}
