package com.hashted.examples.guessthenumber.entity.guessnumber;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class Checker {
    static private List<Integer> convertIntNumberToList(int value) {
        List<Integer> list = new ArrayList<>();
        int t = value;
        while (t > 0) {
            list.add(t % 10);
            t /= 10;
        }
        Collections.reverse(list);
        return list;
    }

    static private boolean checkListSize(List<Integer> list, int length) {
        return list.size() == length;
    }

    static private boolean findDuplicateValues(List<Integer> list) {
        HashSet<Integer> set = new HashSet<>(list);
        return list.size() != set.size();
    }

    static private Herd checkMatches(List<Integer> firstList, List<Integer> secondtList) {
        if (firstList.equals(secondtList))
            return new Herd(4,0);

        int b = 0, c = 0;
        for (int i = 0; i < secondtList.size(); i++) {
            if (secondtList.get(i) == firstList.get(i))
                b++;
            else if (firstList.contains(secondtList.get(i)))
                c++;
        }
        return new Herd(b, c);
    }

    static public Attempt checkUserAttempt(List<Integer> generated, int number) {
        List<Integer> list = convertIntNumberToList(number);

        if (!checkListSize(list, generated.size()))
            return new Attempt(number, new Herd(0,0), AttemptStatus.INVALID_LENGTH);
        if (findDuplicateValues(list))
            return  new Attempt(number, new Herd(0,0), AttemptStatus.REPEATED_DIGITS);

        Herd result = checkMatches(generated, list);

        if ((result.getBulls() + result.getCows() >= 0) && (result.getBulls() + result.getCows() <= 4)) {
            if(result.getBulls() == 4)
                return new Attempt(number, result, AttemptStatus.CORRECT);
            else
                return new Attempt(number, result, AttemptStatus.WRONG);
        } else
            return new Attempt(number, result, AttemptStatus.UNEXPECTED_RESULT);
    }
}
