package com.hashted.examples.guessthenumber.entity;

import lombok.Data;

@Data
public class RatingInfo {
    private String name;
    private double avgNumOfAttempts;
    private int place;

    public RatingInfo(String name, double avgNumOfAttempts, int place) {
        this.name = name;
        this.avgNumOfAttempts = avgNumOfAttempts;
        this.place = place;
    }
}
