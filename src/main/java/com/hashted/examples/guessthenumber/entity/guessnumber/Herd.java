package com.hashted.examples.guessthenumber.entity.guessnumber;

import java.util.Objects;

public class Herd {
    private int bulls = 0;
    private int cows = 0;

    public Herd(int bulls, int cows) {
        this.bulls = bulls;
        this.cows = cows;
    }

    public int getBulls() {
        return bulls;
    }

    public void setBulls(int bulls) {
        this.bulls = bulls;
    }

    public int getCows() {
        return cows;
    }

    public void setCows(int cows) {
        this.cows = cows;
    }

    @Override
    public String toString() {
        return bulls + "B" + cows + "C";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Herd herd = (Herd) o;
        return bulls == herd.bulls &&
                cows == herd.cows;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bulls, cows);
    }
}
