package com.hashted.examples.guessthenumber.entity.guessnumber;

public class Attempt {
    private int value;
    private Herd checkResult;
    private AttemptStatus status;

    public Attempt(int value, Herd checkResult, AttemptStatus status) {
        this.value = value;
        this.checkResult = checkResult;
        this.status = status;
    }


    @Override
    public String toString() {
        return "Attempt{" +
                "value=" + value +
                ", checkResult=" + checkResult +
                ", status=" + status +
                '}';
    }

    public int getValue() {
        return value;
    }

    public Herd getCheckResult() {
        return checkResult;
    }

    public AttemptStatus getStatus() {
        return status;
    }
}
