package com.hashted.examples.guessthenumber.entity.guessnumber;

public enum AttemptStatus {
    CORRECT, WRONG, REPEATED_DIGITS, INVALID_LENGTH, UNEXPECTED_RESULT, ILLEGAL_TYPE;
}
