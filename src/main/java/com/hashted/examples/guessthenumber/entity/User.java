package com.hashted.examples.guessthenumber.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data   //Generates getters and setters for all fields, a toString method, hashCode and equals implementations.
        // Equivalent to @Getter @Setter @RequiredArgsConstructor @ToString @EqualsAndHashCode.
@Entity
@Table(name = "user")
public class User {

    @Id //primary key
    @GeneratedValue(strategy = GenerationType.AUTO) //Hibernate selects a generation strategy based on the database.
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "login")
    @Length(min = 3, message = "* Login must be at least 3 characters long.")
    @NotEmpty(message = "Please provide your login.")
    private String login;

    @Column(name = "pass")
    @Length(min = 4, message = "* Password must be at least 3 characters long.")
    @NotEmpty(message = "Please provide your password.")
    private String pass;

    @Column(name = "active")
    private int active;

    @ManyToMany(cascade = CascadeType.ALL)
        //The users table is in “many to many” relationship with the role table.
        //The relationship includes all operations as cascading (PERSIST, MERGE, REFRESH, REMOVE, DETACH).
    @JoinTable(name = "user_role", //Interaction with a intermediary table
            joinColumns = {@JoinColumn( //columns mapping of the owning side
                    name = "user_id", //column name of the intermediary table
                    referencedColumnName = "id")}, //primary key column name of the owning side
            inverseJoinColumns = {@JoinColumn( //columns mapping of the inverse side
                    name = "role_id", //column name of the intermediary table
                    referencedColumnName = "id")}) //foreign key
    private Set<Role> roles;
}
