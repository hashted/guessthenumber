package com.hashted.examples.guessthenumber.repository;

import com.hashted.examples.guessthenumber.entity.Stat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatRepository extends JpaRepository<Stat, Long> {

    @Query(
            value = "SELECT login, avg_value, ROWNUM AS place FROM " +
                    "(SELECT TOP :top u.login, AVG(CAST(s.attempts_quantity AS double)) AS avg_value " +
                    "FROM stat s INNER JOIN user u ON (s.user_id = u.id) GROUP BY s.user_id ORDER BY avg_value)",
            nativeQuery = true
    )
    List<Object[]> getTopByAvg(@Param("top") int top);

    @Query(
            value = "SELECT AVG(CAST(attempts_quantity AS double)) FROM stat WHERE user_id  = :id GROUP BY user_id",
            nativeQuery = true
    )
    Object getAvgByUserId(@Param("id") int user_id);
}
