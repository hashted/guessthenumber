package com.hashted.examples.guessthenumber.controller;

import com.hashted.examples.guessthenumber.entity.User;
import com.hashted.examples.guessthenumber.entity.guessnumber.Attempt;
import com.hashted.examples.guessthenumber.service.StatService;
import com.hashted.examples.guessthenumber.service.UserService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;

@Controller
public class LoginController {

    @Autowired
    private HttpSession httpSession;

    @Value("${web.root}")
    private String webRoot;

    @Autowired
    private UserService userService;

    @Autowired
    private StatService statService;


    @RequestMapping(value={"/", "/login"}, method = RequestMethod.GET)
    public ModelAndView login(){

        var modelAndView = new ModelAndView();

        modelAndView.addObject("webRoot", webRoot);
        modelAndView.setViewName("thymeleaf/login");

        return modelAndView;
    }

    @RequestMapping(value={"/registration"}, method = RequestMethod.GET)
    public ModelAndView registration(){
        var user = new User();

        var modelAndView = new ModelAndView();

        modelAndView.addObject("webRoot", webRoot);
        modelAndView.addObject("user", user);
        modelAndView.setViewName("thymeleaf/registration");

        return modelAndView;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView createUser(@Valid User user, BindingResult bindingResult) {

        var modelAndView = new ModelAndView();

        User userExists = userService.findUserByLogin(user.getLogin());
        if (userExists != null) {
            bindingResult
                    .rejectValue("login", "error.user",
                            "There is already a user registered with the login provided.");
        }

        if (!bindingResult.hasErrors()) {
            userService.saveUser(user);
            modelAndView.addObject("successMessage", "User has been registered successfully.");
            modelAndView.addObject("user", new User());
        }

        modelAndView.addObject("webRoot", webRoot);
        modelAndView.setViewName("thymeleaf/registration");

        return modelAndView;
    }

    @RequestMapping(value="/guessnumber", method = RequestMethod.GET)
    public ModelAndView guessnumber() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByLogin(auth.getName());

        httpSession.setAttribute("user", user);
        httpSession.setAttribute("generatedList", new ArrayList<Integer>());
        httpSession.setAttribute("attemptList", new ArrayList<Attempt>());

        var modelAndView = new ModelAndView();

        modelAndView.addObject("webRoot", webRoot);
        modelAndView.addObject("userName", user.getLogin());
        modelAndView.setViewName("guessnumber.jsp");

        return modelAndView;
    }

    @RequestMapping(value={"/rating"}, method = RequestMethod.GET)
    public ModelAndView rating(){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByLogin(auth.getName());

        var modelAndView = new ModelAndView();

        modelAndView.addObject("webRoot", webRoot);
        modelAndView.addObject("userName", user.getLogin());
        modelAndView.addObject("avgNumOfAttempts", statService.getAvgNumOfAttempts(user.getId()));
        modelAndView.addObject("ratingList", statService.getRatingList());
        modelAndView.setViewName("thymeleaf/rating");

        return modelAndView;
    }
}
